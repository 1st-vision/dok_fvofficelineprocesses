.. |label| replace:: Vorgänge und Belege im Kundenkonto / Gleichgewichtsstatistik
.. |snippet| replace:: FvOfficeLineProcesses
.. |Author| replace:: 1st Vision GmbH
.. |Entwickler| replace:: Tristan Hahner
.. |minVersion| replace:: 5.3.4
.. |maxVersion| replace:: 5.3.4
.. |Version| replace:: 1.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Das Modul ersetzt die gewöhnliche Bestellübersicht im Account-Frontend-Controller des Shops. 
Es werden sowohl Shop-Bestellungen angezeigt als auch Bestellungen, die nicht über den Shop erfolgt sind. Hierbei ist die Auftragsbestätigung ausschlaggebend bzw. die Belegart, die „Bestellt = 1“ in der Gleichgewichtsstatistik der Office Line angegeben hat. Wenn eine Bestellung im Shop existiert, wird die Bestellnummer des Shops angezeigt. Andernfalls die Belegnummer der AB.

Frontend
--------
Der Bestellstatus richtet sich nach dem Bestellstatus des Shops. Bei Bestellungen, die nicht über den Shop erfolgt sind, ist kein Bestellstatus angegeben.
Neue Bestellübersicht:

.. image:: FvOfficeLineProcesses1.png

Die Detailansicht bei Klick auf den Button „Anzeigen“ wurde um die Angabe „Geliefert“ erweitert. Hierbei werden alle Geliefert-Belege einbezogen und aufsummiert. Im Folgenden Beispiel fäll t sofort auf, dass Pos. 1 zu oft geliefert wurde.

Button „Anzeigen“:

.. image:: FvOfficeLineProcesses2.png

Weiterhin werden zusätzlich erfolgte Lieferungen angezeigt. Hierbei werden alle Belege herangezogen, die in der OL Gleichgewichtsstatistik „Geliefert = 1“ oder „Geliefert = -1“ angegeben haben.

Button „Lieferung“:

.. image:: FvOfficeLineProcesses3.png

Backend
-------
Funktion: Status E-Mails „Komplett ausgeliefert“ und „Teilweise ausgeliefert“:

Das Plugin legt bei der Installation einen deaktivierten Shopware Cronjob an, der Status-Mails zu Lieferungen versenden kann. Für jeden Lieferschein wird eine E-Mail mit den im Lieferscheint enthaltenen Positionen versendet. Das Plugin differenziert hierbei automatisch zwischen Teillieferungen und Komplett-Lieferungen.

Es werden die im Shopware Standard bereits vorhandenen Mail-Templates sORDERSTATEMAIL6 und sORDERSTATEMAIL7 verwendet. Diese müssen allerdings noch konfiguriert werden bzw. der Inhalt den Variablen angepasst werden.
Im Plugin-Verzeichnis liegen Beispiel-Templates, die herangezogen werden können. Diese sind zu finden unter:

custom/plugins/FvOfficeLineProcesses/sORDERSTATEMAIL6.tpl
custom/plugins/FvOfficeLineProcesses/sORDERSTATEMAIL7.tpl

In den Templates können folgende Variablen verwendet werden:

:$deliveryDocument: Beinhaltet den Lieferschein aus der Tabelle fv_office_line_document.

:$positions: Beinhaltet die Positionen des Lieferscheint aus der Tabelle fv_office_line_document_position.

:$user: Beinhaltet die Daten des Kunden aus der Tabelle s_user.

:$previousDeliveries: Anzahl der bereits zu diesem Vorgang erfolgten Lieferungen.

:$order: Sofern die Bestellung im Shop vorhanden ist, beinhaltet diese Variable die Bestellung aus s_order.

:$order[‚positions‘]: Sofern die Bestellung im Shop vorhanden ist, beinhaltet diese Variable die Bestell-Positionen aus s_order_details.

:$orderDocument: Sofern die Bestellung im Shop nicht vorhanden ist, beinhaltet diese Variable die Auftragsbestätigung (bzw. den Beleg der Bestellung).

:$orderDocument[‚positions‘]: Sofern die Bestellung im Shop nicht vorhanden ist, beinhaltet diese Variable die Positionen der Auftragsbestätigung.

Über die Plugin-Konfiguration können die Auswirkungen der Belegarten aus der OL-Tabelle KHKVKBelegarten angegeben werden bzw. nachgebildet werden. Die in der OL-Tabelle angegebenen Werte für „Bestellt“, „Geliefert“ und „Berechnet“ sollten 1:1 in die Plugin-Konfiguration übertragen werden.
Nach der Installation ist bereits der OL-Standard abgebildet.

.. image:: FvOfficeLineProcesses4.png

technische Beschreibung
------------------------
Voraussetzung ist die korrekte Konfiguration von OLSI und Connector zur Übertragung der Belege und Vorgänge von der Office Line in den Shop.
Die Datenübertragung der Belege und Vorgänge erfolgt über OLSI bzw. Connector. Sämtliche Funktionen, die das Frontend des Shops betreffen, sind in diesem Modul verbaut.

Das Modul bezieht seine Daten aus den vom Connector angelegten Tabellen:

.. image:: FvOfficeLineProcesses5.png

Modifizierte Template-Dateien
-----------------------------
:/account/sidebar.tpl:


kompatibel mit anderen Plugins
------------------------------

:User-Berechtigungen für Konzerne: FvUserPermissions

